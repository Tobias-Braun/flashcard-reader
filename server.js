const express = require('express');
const app = express();
const path = require('path');
const api  = require('./server/routes/api/api');
const http = require('http');
const bodyParser = require('body-parser')
const mongoose = require('mongoose');
const config = require('./config');
const cors = require('cors');

mongoose.connect(config.database.url);

mongoose.connection.on('connected', function() {
  console.log('connected to db');
});

mongoose.connection.on('error', function(err) {
  console.log(err);
});

app.use('/api', api);
app.use(express.static(path.join(__dirname, 'dist')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('*', function(req, res){
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

const server = http.createServer(app);
server.listen(3000);
console.log('app running on port 3000');
