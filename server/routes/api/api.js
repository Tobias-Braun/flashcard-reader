const express = require('express');
const router = express.Router();
const mongojs = require('mongojs');
const db = mongojs('flashcards', ['flashcards']);
router.get('/', function(req, res){
  res.send('this is nice');
});

router.get('/cards', function(req, res){

  db.flashcards.find(function(err, docs){
    if (err) console.log(err);
    else res.json(docs);
  });
});

module.exports = router;
