import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [HttpClient]
})

export class AppComponent {
  constructor(private http: HttpClient) {}
  card = {
    front: 'front',
    back: 'back',
    title: 'title'
  };
  showsFront = true;

  cardClicked() {
    this.showsFront = !this.showsFront;
    console.log(this.showsFront);
    this.http.get('api/cards').subscribe(data => {
      const cardinfo = data[Math.floor(Math.random() * 2.5)];
      this.card.back = cardinfo.back;
      this.card.front = cardinfo.front;
    });
  }
}
